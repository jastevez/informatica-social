export class AdultoDTO {
  constructor(
    public telefono_1: string,
    public telefono_celular: string,
    public cuenta_correo: string,
    public estado_civil: string,
    public direccion: string,
    public barrio: string,
    public estudios: string,
    public seguro_medico: string
  ) {  }
}
