export class PersonaDTO {
  constructor(
    public tipo_identificacion: number,
    public numero_id: number,
    public fecha_exp: Date,
    public nombre1: string,
    public nombre2: string,
    public apellido1: string,
    public apellido2: string,
    public lugar_exp: Date,
    public sexo: string,
    public fecha_nac: string,
    public programa: number,
    public estado: string,
    public asistio?: boolean
  ) {  }
}
