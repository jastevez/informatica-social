export class RehabilitadoDTO {
  constructor(
    public tiempo_consumo: number,
    public tipo_alucinogeno: string,
    public compromiso: string,
    public observaciones: string
  ) {  }
}