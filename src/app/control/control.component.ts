import { Component, OnInit, NgZone } from '@angular/core';
declare const db: any;
declare const $: any;
declare const jsPDF: any;
declare const base64: any;
@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit {
  active = true;
  fecha1: Date;
  fecha2: Date;
  model = {
    descripcion: null,
    tipo: null,
    valor: null,
    fecha: null
  };
  lista = [];
  ingreso = 0;
  gasto = 0;
  constructor(private zone: NgZone) { }

  ngOnInit() {
    
  }
  onSubmit(){
    let context = this;
     db.run("INSERT INTO GastosIngresos (valor, detalle, tipo, fecha ) VALUES (?,?,?,?)", 
     [this.model.valor, this.model.descripcion, this.model.tipo, this.model.fecha],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.model = {
            descripcion: null,
            tipo: null,
            valor: null,
            fecha: null
          };
          //context.onLoad();
          //context.loadSum();
       }
     });
     });
  }
  change2(){
    this.lista = [];
    this.fecha1 = null;
    this.fecha2 = null;
    this.change();
  }
  change(){
    this.active = !this.active;
  }
  listar(){
    if(this.fecha1 <= this.fecha2){
      this.onLoad();
      this.loadSum();
    }
  }
  onLoad(){
    this.lista = [];
    let context = this;
    db.each("SELECT valor, detalle, tipo, fecha FROM GastosIngresos where fecha between ? and ? order by fecha, tipo ",
    [this.fecha1, this.fecha2], function(err, row) {
       context.zone.run(() => {
          context.lista.push({fecha: row.fecha, descripcion: row.detalle, tipo: row.tipo, valor: row.valor });
       });
    });
  }
  loadSum(){
     let context = this;
      db.each("SELECT SUM(valor) as valor, tipo FROM GastosIngresos where fecha between ? and ? group by tipo",
       [this.fecha1, this.fecha2], function(err, row) {
         context.zone.run(() => {
            if(row.tipo === 'I'){
              context.ingreso = row.valor;
            }else if(row.tipo === 'G'){
              context.gasto = row.valor;
            }
         });
      });
  }
  imprimir(){
    this.pruebaDivAPdf();
  }
  pruebaDivAPdf() {
    let context = this;
        var pdf = new jsPDF('p', 'pt', 'letter');
        let source = $('#window-pdf')[0];
        $( "#window-pdf" ).prepend( '<p id="arg3">entre ' + this.fecha1 + ' y ' + this.fecha2 +'</p>' );
        $( "#window-pdf" ).prepend( '<p id="arg2">Reporte de Ingresos y Gastos</p>' );
        $( "#window-pdf" ).prepend( '<p id="arg1">Fundación Roca de Sion</p>' );
        let specialElementHandlers = {
            '#bypassme': function (element, renderer) {
                return true
            }
        };
        let margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };

        pdf.fromHTML(
            source, 
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, 
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
              pdf.output('blob');
              //var out = pdf.output();
              pdf.save('ingresos_y_gastos.pdf');
               $( "#arg1" ).remove();
               $( "#arg2" ).remove();
               $( "#arg3" ).remove();
            }, margins
        );
    }
}
