import { AuthGuard } from './guards/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';

import { AppComponent } from './main.component/app.component';
import { LoginComponent } from './main/login/login.component';
import { RegistroComponent } from './main/registro/registro.component';
import { BuscarComponent } from './main/buscar/buscar.component';
import { CamaraComponent } from './camara/camara.component';
import { AsistenciaComponent } from './asistencia/asistencia.component';
import { ControlComponent } from './control/control.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { ProgramasComponent } from './programas/programas.component';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { IdentifiacionesComponent } from './identifiaciones/identifiaciones.component';
import { CameraComponent } from './main/camera/camera.component';
import { HomeComponent } from './main/home/home.component';
import { SearchPipe } from "./pipes/filter";
import { ReporteBeneficiarioComponent } from './reporte-beneficiario/reporte-beneficiario.component';
import { HistorialAsistenciaComponent } from './historial-asistencia/historial-asistencia.component';

@NgModule({
  declarations: [
    SearchPipe,
    AppComponent,
    LoginComponent,
    RegistroComponent,
    BuscarComponent,
    CamaraComponent,
    AsistenciaComponent,
    ControlComponent,
    ActividadesComponent,
    ProgramasComponent,
    CiudadesComponent,
    DepartamentosComponent,
    IdentifiacionesComponent,
    CameraComponent,
    HomeComponent,
    ReporteBeneficiarioComponent,
    HistorialAsistenciaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: '.login',
        component: LoginComponent
      },
      {
        path: '.registro',
        component: RegistroComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.reporte-beneficiario',
        component: ReporteBeneficiarioComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.buscar',
        component: BuscarComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.camera',
        component: CameraComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.lista',
        component: AsistenciaComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.historial-asistencia',
        component: HistorialAsistenciaComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.financiero',
        component: ControlComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.actividades',
        component: ActividadesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.ciudades',
        component: CiudadesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.departamentos',
        component: DepartamentosComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.identificaciones',
        component: IdentifiacionesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '.programas',
        component: ProgramasComponent,
        canActivate: [AuthGuard]
      }
    ])
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
