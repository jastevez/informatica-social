
import { Pipe, PipeTransform, Injectable} from '@angular/core';
@Pipe({
    name: 'filterPipe'
})
@Injectable()
export class SearchPipe implements PipeTransform {
  public transform(value: any, key: string, term: string) {
    return value.filter((item: any) => {
      if (item.hasOwnProperty(key)) {
        if (term) {
          let regExp = new RegExp('\\b' + term, 'gi');
          return regExp.test(item[key]);
        } else {
          return true;
        }
      } else {
        return false;
      }
    });
  }
}