import { Component, NgZone, OnInit } from '@angular/core';
import { PersonaDTO } from "../dto/persona";
declare const db: any;
declare const $: any;
declare const jsPDF: any;
declare const base64: any;
@Component({
  selector: 'app-historial-asistencia',
  templateUrl: './historial-asistencia.component.html',
  styleUrls: ['./historial-asistencia.component.css']
})
export class HistorialAsistenciaComponent implements OnInit {
   model: PersonaDTO[] = [];
   fecha_asistencia = '';
   actividad: any = [];
   actividad_sel = '';
   active = false;
  constructor(private zone: NgZone) {
    this.loadActividad();
   }

  ngOnInit() {
  }
  loadActividad(){
    this.actividad = [];
    let context = this;
    db.each("select asist.id_actividad as id_actividad, max(act.nombre) as nombre, max(act.fecha) as fecha from Asistencia asist, Actividad act where  asist.id_actividad = act.id_actividad group by asist.id_actividad ",
     function(err, row) {
       context.zone.run(() => {
          context.actividad.push({id: row.id_actividad, detalle: row.nombre, fecha: row.fecha});
       });
    });
  }
  generarLista(){
    this.active = !this.active;
    this.loadList();
  }
  reset(){
    this.active = !this.active;
    this.model = [];
    this.actividad_sel = null;
    this.loadActividad();
  }
  loadList(){
    let context = this;
    context.model = [];
    console.log(this.actividad_sel);
    db.each("select ps.Numero_documento as num, ps.nombre1, ps.nombre2, ps.apellido1, ps.apellido2, asist.asistio  from Asistencia asist, Persona ps where  asist.id_cedula = ps.Numero_documento and asist.id_actividad = ?", [this.actividad_sel], function(err, row) {
       context.zone.run(() => {
         if(row !== undefined){
           let name = row.nombre1;
           if(row.nombre2 != null)
             name += ' ' + row.nombre2;
           if(row.apellido1 != null)
             name += ' ' + row.apellido1;
           if(row.apellido2 != null)
             name += ' ' + row.apellido2;
            context.model.push(new PersonaDTO(null, row.num,null,name, null, null, null, null,null, null,null,null,row.asistio === 'S'?true:false));
         }
       });
    });
  }
  imprimir(){
    this.pruebaDivAPdf();
  }
  pruebaDivAPdf() {
    let context = this;
        var pdf = new jsPDF('p', 'pt', 'letter');
        let source = $('#window-pdf')[0];
       for(let i = 0;i<this.actividad.length;i++){
         if(Number(this.actividad[i].id) === Number(this.actividad_sel)){
           let data = this.actividad[i].detalle + ' - ' + this.actividad[i].fecha;
            $( "#window-pdf" ).prepend( '<p id="arg3">'+data+'</p>' );
         }
       }
        $( "#window-pdf" ).prepend( '<p id="arg2">Listado de Asistencia</p>' );
        $( "#window-pdf" ).prepend( '<p id="arg1">Fundación Roca de Sion</p>' );
        let specialElementHandlers = {
            '#bypassme': function (element, renderer) {
                return true
            }
        };
        let margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };

        pdf.fromHTML(
            source, 
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, 
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
              pdf.output('blob');
              //var out = pdf.output();
              pdf.save('lista.pdf');
               $( "#arg1" ).remove();
               $( "#arg2" ).remove();
               $( "#arg3" ).remove();
            }, margins
        );
    }
}
