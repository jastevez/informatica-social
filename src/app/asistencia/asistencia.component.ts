import { Component, OnInit,NgZone } from '@angular/core';
import { PersonaDTO } from './../dto/persona';
declare const db: any;
@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.css']
})
export class AsistenciaComponent implements OnInit {
 model: PersonaDTO[] = [];
 fecha_asistencia = '';
 programa: any = [];
 programa_sel = '';
 actividad: any = [];
 actividad_sel = '';
 active = false;
  constructor(private zone: NgZone) { 
    this.loadPrograma();
    this.loadDate();
    this.loadActividad();
  }

  ngOnInit() {
  }
  generarLista(){
    this.active = !this.active;
    this.loadList();
  }
  guardarLista(){
    this.active = !this.active;
    this.saveList();
    this.loadActividad();
  }
  reset(){
    this.active = !this.active;
    this.model = [];
    this.actividad_sel = null;
    this.programa_sel = null;
    this.loadActividad();
  }
  loadList(){
    let context = this;
    context.model = [];
    var funcData = function(err, row) {
       context.zone.run(() => {
         let name = row.nombre1;
         if(row.nombre2 != null)
           name += ' ' + row.nombre2;
         if(row.apellido1 != null)
           name += ' ' + row.apellido1;
         if(row.apellido2 != null)
           name += ' ' + row.apellido2;
          context.model.push(new PersonaDTO(null, row.num,null,name, null, null, null, null,null, null, null, null));
       });
    };
    if(this.programa_sel === null || this.programa_sel.length === 0){
      db.each("SELECT Numero_documento as num, nombre1, nombre2, apellido1, apellido2 FROM Persona where estado = 'A'",funcData 
    );
    }else{
      db.each("SELECT Numero_documento as num, nombre1, nombre2, apellido1, apellido2 FROM Persona where estado = 'A' and programa like ?",[this.programa_sel],funcData);
    }
  }
  saveList(){
    console.log(this.model);
    for(let i = 0;i< this.model.length ;i++){
        this.create(this.model[i].numero_id, Number(this.actividad_sel) ,this.model[i].asistio);
    }
  }
  create(ced: number, act: number, ast: boolean){
    let context = this;
     db.run("INSERT INTO Asistencia (id_cedula,id_actividad,asistio, fecha)VALUES (?,?,?,?)", 
     [ced, act, ast?'S':'N', this.fecha_asistencia],(err)=>{
       if(err){
           throw err;
       }
     });
  }
  loadPrograma(){
    this.programa = [];
    let context = this;
    db.each("SELECT id_programa, nombre FROM Programa where estado = 'A' ",
     function(err, row) {
       context.zone.run(() => {
          context.programa.push({id: row.id_programa, detalle: row.nombre});
       });
    });
  }
  loadActividad(){
    this.actividad = [];
    let context = this;
    db.each("SELECT id_actividad, nombre FROM Actividad act where (select count(*) from Asistencia ast where ast.id_actividad = act.id_actividad) = 0 ",
     function(err, row) {
       context.zone.run(() => {
          context.actividad.push({id: row.id_actividad, detalle: row.nombre});
       });
    });
  }
  loadDate(){
    let today = new Date();
    let month = today.getUTCMonth() + 1; // months from 1-12
    let day = today.getUTCDate() - 1;
    let year = today.getUTCFullYear();
    this.fecha_asistencia = year + '-' + (month < 10?'0':'')  + month + '-' + (day < 10?'0':'') + day;
  }
}
