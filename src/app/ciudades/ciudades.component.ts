import { Component, OnInit, NgZone } from '@angular/core';
import {Observable} from 'rxjs/Rx';

declare const db: any;
@Component({
  selector: 'app-ciudades',
  templateUrl: './ciudades.component.html',
  styleUrls: ['./ciudades.component.css']
})
export class CiudadesComponent implements OnInit {
lista = [];
  model = {
    id: null,
    nombre: null,
    departamento: null
  };
  departamentos = [];
  constructor(private zone: NgZone) { 
    this.loadDepartamento();
    this.onLoadList();
  }

  ngOnInit() {
  }

  onSubmit(){
      if(this.model.id !== null ){
        this.update();
      }else{
        this.create();
      }
  }
  loadDepartamento(){
    this.departamentos = [];
    let context = this;
    db.each("SELECT id_departamento, nombre FROM Departamento order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.departamentos.push({id: row.id_departamento, nombre: row.nombre });
       });
    });
  }
  onLoadList(){
    this.lista = [];
    let context = this;
    db.all("SELECT id_ciudad, nombre, id_departamento FROM Ciudad order by nombre",
    function(err,rows){
      context.zone.run(() => {
        for(let i=0;i<rows.length;i++)
          context.lista.push({id: rows[i].id_ciudad, nombre: rows[i].nombre, departamento: rows[i].id_departamento });
           Observable.timer(1000).subscribe(val =>{ // ajuste para la actualizacion de la vista
            context.zone.run(() => {
            });
          });
        });
    });
  }
  findDep(id: number): string{
    for(let i=0;i<this.departamentos.length;i++){
        if(this.departamentos[i].id === id){
          return this.departamentos[i].nombre;
        }
    }
    return "";
  }
  create(){
    let context = this;
     db.run("INSERT INTO Ciudad ( nombre, id_departamento ) VALUES (?,?)", 
     [this.model.nombre, this.model.departamento ],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  setRow(index: number){
    this.model = {
      id: this.lista[index].id,
      nombre: this.lista[index].nombre,
      departamento: this.lista[index].departamento
    };
  }
  update(){
    let context = this;
     db.run("UPDATE Ciudad SET nombre = ?, id_departamento = ? WHERE id_ciudad = ? ", 
     [this.model.nombre, this.model.departamento, this.model.id],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  init(){
     this.model = {
      id: null,
      nombre: null,
      departamento: null
    };
  }
}
