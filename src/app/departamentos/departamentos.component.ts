import { Component, OnInit, NgZone } from '@angular/core';
declare const db: any;
@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.css']
})
export class DepartamentosComponent implements OnInit {
  lista = [];
  model = {
    id: null,
    nombre: null
  };
  constructor(private zone: NgZone) { }

  ngOnInit() {
    this.onLoadList();
  }

  onSubmit(){
      if(this.model.id !== null ){
        this.update();
      }else{
        this.create();
      }
  }
  onLoadList(){
    this.lista = [];
    let context = this;
    db.each("SELECT id_departamento, nombre FROM Departamento order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.lista.push({id: row.id_departamento, nombre: row.nombre });
       });
    });
  }
  create(){
    let context = this;
     db.run("INSERT INTO Departamento ( nombre) VALUES (?)", 
     [this.model.nombre ],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  setRow(index: number){
    this.model = {
      id: this.lista[index].id,
      nombre: this.lista[index].nombre
    };
  }
  update(){
    let context = this;
     db.run("UPDATE Departamento SET nombre = ? WHERE id_departamento = ? ", 
     [this.model.nombre, this.model.id],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  init(){
     this.model = {
      id: null,
      nombre: null
    };
  }
}
