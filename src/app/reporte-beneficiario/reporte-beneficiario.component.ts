import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaDTO } from "../dto/persona";
import { AdultoDTO } from "../dto/adulto";
import { RehabilitadoDTO } from "../dto/rehabilitado";
declare const db: any;
@Component({
  selector: 'app-reporte-beneficiario',
  templateUrl: './reporte-beneficiario.component.html',
  styleUrls: ['./reporte-beneficiario.component.css']
})
export class ReporteBeneficiarioComponent implements OnInit {
  model: PersonaDTO = new PersonaDTO(null, null, null, null, null, null, null, null, null, null, null, null);
  adulto: AdultoDTO = new AdultoDTO(null, null, null, null, null, null, null, null);
  rehabilitado: RehabilitadoDTO = new RehabilitadoDTO( null, null, null, null);
  institucion_educativa:string = null;
  grado_cursado:string = null;
  religion: string = null;
  programa = [];
  tipo_id = [];
  image: string;
  constructor(private zone: NgZone, private router: Router, private route: ActivatedRoute) { 
    this.loadId();
    this.loadPrograma();
     route.params.subscribe(params => {
          if (params['id']) {
            this.model.numero_id = params['id'];
            this.getPersona();
          }
    });
  }

  ngOnInit() {
  }
  loadPrograma(){
    let context = this;
    db.each("SELECT id_programa, nombre FROM Programa where estado = 'A' ",
     function(err, row) {
       context.zone.run(() => {
          context.programa.push({id: row.id_programa, detalle: row.nombre});
       });
    });
  }
  loadId(){
    let context = this;
    db.each("SELECT id_documento, nombre FROM TipoDocumento order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.tipo_id.push({id: row.id_documento, detalle: row.nombre });
       });
    });
  }
  getPrograma(val: number): string{
    for(let i = 0;i<this.programa.length;i++){
      if(val === this.programa[i].id){
        return this.programa[i].detalle;
      }
    }
    return "";
  }
  getId(val: number): string{
    for(let i = 0;i<this.tipo_id.length;i++){
      if(val === this.tipo_id[i].id){
        return this.tipo_id[i].detalle;
      }
    }
    return "";
  }
  getEstadoCivil(val: string): string{
    let estado_civil = [
    {id: "1", detalle: "Soltero/a"},
    {id: "2", detalle: "Comprometido/a"},
    {id: "3", detalle: "Casado/a"},
    {id: "4", detalle: "Divorciado/a"},
    {id: "5", detalle: "Viudo/a"},
    {id: "6", detalle: "Otro"}
  ];
    for(let i = 0;i<estado_civil.length;i++){
      if(val === estado_civil[i].id){
        return estado_civil[i].detalle;
      }
    }
    return "";
  }
  getSexo(val: string): string{
    if(val === "M"){
      return "Masculino";
    }else if(val === "F"){
      return "Femenino";
    }
    return "";
  }
  getEstado(val: string): string{
    if(val === 'I'){
      return "Inactivo";
    }else if(val === 'A'){
      return "Activo";
    }
    return "";
  }
  back(){
    this.router.navigate(['.buscar']);
  }
  edit(){
    this.router.navigate(['.registro', {update: 'S', id: this.model.numero_id}]);
  }
  //TODO: Selects
  getPersona(){
     let context = this;
    db.each("SELECT Numero_documento,tipo_doc,nombre1,nombre2,apellido1,apellido2,programa,fecha_nacimiento,sexo,estado,fecha_expedicion,lugar_expedicion FROM Persona where Numero_documento = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.model = new PersonaDTO(row.tipo_doc, row.Numero_documento, row.fecha_expedicion, row.nombre1, row.nombre2, row.apellido1, row.apellido2, row.lugar_expedicion, row.sexo, row.fecha_nacimiento, row.programa, row.estado);
          context.loadFoto();
          if(context.model.programa === 1 || context.model.programa === 2){
            context.getAdulto();
            if(context.model.programa === 1){
              context.getMadre();
            }else if(context.model.programa === 2){
              context.getRehabilitado();
            }
          }else if(context.model.programa === 3){
            context.getNino();
          }
       });
    });
  }
  getAdulto(){
     let context = this;
    db.each("SELECT telefono, celular, email, estado_civil, direccion_domicilio, barrio_domicilio, estudios_cursados, seguro_medico, num_doc FROM Adulto where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.adulto = new AdultoDTO(row.telefono, row.celular, row.email, row.estado_civil, row.direccion_domicilio, row.barrio_domicilio, row.estudios_cursados, row.seguro_medico);
       });
    });
  }
  getNino(){
     let context = this;
    db.each("SELECT institucion_educativa, grado_cursado, num_doc FROM Nino where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.institucion_educativa = row.institucion_educativa;
          context.grado_cursado = row.grado_cursado;
       });
    });
  }
  getRehabilitado(){
     let context = this;
    db.each("SELECT tiempo_consumo,tipo_alucinogeno,compromiso,observaciones,num_doc FROM Rehabilitados where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.rehabilitado = new RehabilitadoDTO(row.tiempo_consumo, row.tipo_alucinogeno, row.compromiso, row.observaciones);
       });
    });
  }
  getMadre(){
     let context = this;
    db.each("SELECT iglesia, num_doc FROM Madre where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.religion = row.iglesia;
       });
    });
  }
  open(){
    window.open(this.image, '_blank');
  }
  loadFoto(){
    let context = this;
    db.all("SELECT foto FROM Foto where num_id=?", [this.model.numero_id],
     function(err, rows) {
       context.zone.run(() => {
          if(rows.length > 0){
            context.image = rows[0].foto;
          }else{
            context.image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS8AAAFACAYAAADtfPpXAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAADa6SURBVHhe7Z0LWyK59nfPJzjf/xO9/+fMdItyBwEVFG94V0C0O29WYGsoi25nmtIq/OWZNQWlbdtVyWInlez8p1LfdUIIUTQkLyFEIZG8hBCFRPISQhQSyUsIUUgkLyFEIZG8hBCFRPISQhQSyUsIUUgkLyFEIZG8hBCFRPISQhQSyUsIUUgkLyFEIZG8hBCFRPISQhQSyUsIUUgkLyFEIZG8hBCFRPISQhQSyUsIUUgkLyFEIZG8hBCFRPISQhQSyUu45u5+oNHec/VWz9WaXVdtdFK/t1xrB3aqrXezXWkuvbefAfHPTJ43+F2M+Hza94qvg+QllgSAIJAXEkNmEH+vfU8M3/9e4j9nPy+Wmv18/n77HWLxpWE/R3wtJC/xEsEksa9b5GTnTC4mt+SfWwU/o1RuuK2duvtWqrq/tyoBzoH9PfZ3mPCSP8d+L/G1kbzEUiQUC+b7di1Ixr4HkSCrVufA7fYGrrt/5HoHw3B8D/wZ/iw/g5/Fz0VGcURm5/g9kBmYxFbBnxFfD8lLvEQ+Fl2ZqCy6GgzPA0cnF+747Mqdjm7c+eWdu7h+cJc3Yzd9cr/k8Xl+HD/+dPeTZ3f7MHPXd9PwZ+Hk/Dr83MPjkTs4PA2ia3f74e+27iPwO8WCM8kl/z3iayB5iRBdEWnxGmkgEGQyurp3V7cTN5m5AAKa/XDu6adz3kd/BD8HqYGd4/XD9Ie7uX8MYjy7uA2i7OwdvkRtSAyBxRFi8t8jvgaSlwhy2Osfh+gKWSAsJBKL5VfEUdavMAkCUZiR9jMRpAmO38dkNjy9XIrOeEqa9m8Sm4/kVSBq//rYdpVGe+VxMn12D94S08cfbuKtMfOi8S/ds2dGlBUdOf/kheL/SPg+jry382lH+3n2Pv55/kvOvwzCes+Rv+/+wUdmN/fufHTthmcXv/33vf86pR9FPpG8CgCNCOq19GO10nIV343iWK21w/m671o16p5mx5XLVVfaLrtSaceVKzXX6fTc6dnITaezIISf/n8/PEU+3j9M3MnJmWvvdl3F/xu3dyrhWK01XI1rs7guv7qOq45p90R8PpJXAaAR/QqkVasupLWA95Vy05V36q7RaLlWq+V6vZ47OjpyFxcX7uHhwT09PfmG/7Pw8O+YTqfu7u4u/NuGw6Hb39/3ku64drvjr8dCXP6aBMFH1wvBx9ctCfKSwPKJ5FUA0hpVDA0xSYjGvLwq5YY7OBj4qOTE3d7ehoZuxRp+UgZF4/Hx0T0/07GcF/5NiOz09NQNBkeu1+273fZ+EBXXpbzTCNeG6yR5FRfJqwCkNaoYa5Q723W3XaqFhtlsdH2jHbj+wdBdX9+68Xjsfvz4sWjer4VGnyaEIhGLywrn+Tff3Ny587Mrdzwcub3eYbguscB4nXZNDckrv0heBSCtUcUgLxpiaasaIKLoHxy7q8t7N50gp0WLXlGSMigaqwpfw9ezx5/u4X7mRuc3btA/CVEY141rhsSS1zNG8sovklcBSGtUMdZN5Nhq9kIDvb56cM/2KG9RaMyz2Sx0s6y7aOeLjBUiMP59sBRl+m/56d9OJz/czfXYnZ5cuoP9YZAYkVjaNTUkr/wieRWAtEYVQ3eR6Ito6/Lizk3Gz+6HzS1IyAtpWQOnsdPIkzIoGvZvi+W11JW06+BBYo/Tny8SOzo8S72mhuSVXySvApDWqGK+fyu7dmsviCtuqAhsMn4doLcSN3SisFgERQQBp0mYc8/PPtr03cYlmS+uDV1JItS0a2pIXvlF8soB/+9/WyG7Amv1WM8HLIFh6ctf38thEJ7oiq4hERbdHQbj9/eOQrRFNPFLfGNNNuyvQ+JapMDYIF1tGzuk693tzJ9QSl75RfLKAZY9wRZGcy5eGE3XZnh07k6OL9z52bW7GN2GiIGuz+3NJLVBLiF5/ZK722m4poeD0yAuBAaMh3W6fckrp0heOYD1ecjK0sBwjoXIZFlgPd/skSdmzj3NXBiEp8tDo7MuULIxvkHy+iWMgQESQ2CIi644D0B6e4eSV06RvHKApXoBIi0WSbNA+m78FBYmx2M1aaQ1yCX4nqUG/ZVIXIsU7DrywUA0S1ecLiTyUrcxv0heOYDULoiLTAmkoiElTJzRISkqIi+gsUGyMb6BP7fUoL8SiWuRgl1brinRLU9rmRPGQ5Bv330ElnLPxOcjeeUE0tIQbaWliInlZaQ1wpXw/UsN+iuRuBYp2PV8mRfnQWKMM7LgXfLKJ5JXDiBDKYn/yKGVFBd5rWyMK9ngjOTX3sD3LDXor0TiWvyC+Joisvu7R3dxeSd55RTJ6yNorMozNT+SR4s8V7QbhGXHkD9r9jMs8XkkOaCHOUs2cA9pYnuD/4HpDfsrkLgWKXBNY3FxnZkfx9d4b/L6t0eRDZJX1ng5kUOr3miF9CyNZtvV682Qd2o4PHHXN3ehga3KU7Ue0hr1VyLtmryf8cPMnZxehnlflhes0eyGAf3Str+3i8msq46p9UL8MZJX1nh5Vat1L6y6azab4Qjk1jo/Pw+ZD9IbnMgHrIl8DqsXeArJ3K/wFNLDpOFK2d9TL6lVIC8JLBskr6yhe0g2z1otUK1Wg8QGg0HIr5XeYER+mA/ejx+ewhNI0uogLVvxoIXdn4fklTVeXnQTkVa5XA5HMnyenZ2FdYWU9EYj8oG/Pz/mY4tMoWCVA8JiyRbysvQ6q5C8skPyypqEvOgy9vt9d3NzExqH5JV3uEFzkBiTWEmnY3JiLlgsqySSV3ZIXlkTycuiLtITTyaTIC5KeqMR+YAb9ArLtBj/QmCsg0wTVozklR2SV9Ys5MV4V6NBPvkDd319vZRvKr3RiHzADVqGMTAE1tk9CN3HNGkZkld2SF5ZE8mLHXzYvYede2gYFCT2tsGI/DAXls35MhjAZ/D+2987qdIyJK/skLyyxsur1dp129vb4SkjY10UEuXZTj7pjUbkA27QfEIwEZfJi/ds6sGUiUp5vhMRsrInkURkYZMPLS/KDMkra7y8ms12GKzf3d0N0yMoNAzJqwhwg+ZPG2N58Z6pEySFRFjx3C+kBbxmUqvklQ2SV9Z4ebHpK08Z2Qj1/v4eX4WyCfnjNx9/ozx0G5EX0jKBkUaa5JCWPgdZEYWZzDjPzuWSVzZIXlnj5cUk1Xa77Y6Pj8OMeis0Dgks73Cj5iAvuov2ntfM/WLgnrlfiIuIC3HxnqeRYRlRWr0Qf4zklTVeXpVKze3t7bnLy8uXialWNGCfd/xN8hB5IatYXnae7KsIzLqMjH0x/yvMAVPklRmSV9Z4ee3sVNzh4WF4yri0JZcvtn+iyCvzrqKBrOInj7wm/z3rHpEW8rJNUkI0pgH7zJC8ssbLa3u7HLqMbDVGg4iLnRN5ZZ4iJ464LAqjGwmMffHk0bqOyIuoi+P2Tl3yygjJK2u8vEqlnTCrnvGtZJG88s48JU78pBF5kQOMbBPk/eL1+dmVazWRF5EX3caeq/mo6/uW0khnheSVNYsBe9Lf2OC8ZtcXDe7TatjY9vHxyZ2djcKT5XKZzCHz+X07/nVIOplWN8QfIXlljZcXM+xHo1GqvPS0sfhwP4mgucesomBOHxOSw+tKTfLKCMkra7y8+BS+uLgIFd2EZUXyKj7IC1izysJ7FuBLXtkjeWWNlxfpn6+urha6oqsxlxhF8io+iIsjT5NZeG+L8BFYpcqAveSVBZJX1nh5dTq9lzWNFCq6Fcmr+NgwAF3H4XAYxGXyqpJFV/LKBMkra7y8er19d3d3Fyp4stintiguJi8KGXLpLpq8amQUkbwyQfLKGi+v/f1+6FKkFcmr+MTyYmyTpWAvG640mKQqeWWB5JU1Xl4HB4OlNY1xkbyKTywvxjbJHiJ5ZY/klTVeXhcXV2E8hMIYl6XCoWh5UPFBXvYEmfdMmUBelUrFdbp7kldGSF5ZI3ltPLG8KHQdw5NGL6/dTk/yygjJK2skr42HexrLi/lejHsx34ud0SWvbJC8skby+hLE8uLJcrfrpVWruWaLHPaSVxZIXlkjeW08lFhePJyxyaqaKpEdklfWSF4bDyWWF/eaXaLCoL1m2GeG5JU1ktfGQ+G+2msG8MnfxkRVrW3MDskraySvjYcSy4sj+duQl1LiZIfklTVeXg8PkyVhUZKVXRSXZOEca1nZt0Dyyg7JK2skr40nWTgneWWP5JU1ktfGkyyck7yyR/LKGslr40kWzkle2SN5ZY3ktfEkC+ckr+yRvLJG8tp4koVzklf2SF5ZI3ltPMnCOckreySvrJG8Np5k4ZzklT2SV9Z4ed3dPbjHx8dF1X5bko1BFAvWMsYJCflgIilhr9fT8qAMkbyyRvLaeOLCe1J+n5ychIyqWh6UHZJX1kheG48VhgZub2+DuOgyam1jtkheWSN5bTx0EyeTiSODar/ff9mAg5Q42vosOySvrJG8Nh62OyMFDrtlI6ydnZ2w5T+vtQFHdkheWSN5bTxhl6DFhhvb29sBySt7JK+skbw2nlKpFISFvIKwvMis26injdkheWWN5LXhOPf9e8l3FSuu4aOs3d2u7z72XKu166qVpittM88rpV6IP0byyhovr5OTs5dkhBSeSjHIS2F+UHqjEPmBOVzcL3YJep1szC28urxz9Vrbs7ug+0p1z0df7B6UUi/EHyN5ZY3ktQGskJc/PZfXQlxeVnMOXqjV5gJLrRvij5C8skby2gAkrzwieWWN5LUBSF55RPLKGslrA5C88ojklTWS1wYgeeURyStrJK8NQPLKI5JX1kheG4DklUckr6zx8mLiIsKixJNVp9NpOKY3GJEXnp4e/fE5iIvXVp7859HpyYXk9UlIXlkjeRWe52ei5nnUJXnlB8kraySvwjPvKpK2+3lJXrPHn+7keCR5fRKSV9ZIXhvAfMwrKa/H6Q83PDqTvD4JyStrJK/Cg7iMeRdyXqaTZ3d0eCp5fRKSV9ZIXoVnlbwm4yd3ODiRvD4JyStrJK/Cs0pe44eZG/SPJa9PQvLKGsmr8IR7FP7v3BOTu3zh/cP9ozvoD4O4SHtDGhxkhcDiY6UueWWB5JU1C3ldX1+HSk+xBkFhsmqysYh8MZ48uUcfeD15b0093D0Udn0zdq2Oj64W93p+RGDI6vX4pk6ItSB5ZY3kVWh++FsV5OVtBVMfQCMuuLh+cM3d/fT7LjJH8soayavQIK+JD7dmPvICySs/SF5ZI3kVGuT1OPvpnvwRYnmds66x1Uu/7yJzJK+skbwKDfKaLYSFvOg6msjOLm5drakxrc9C8soayavQIC8G6i3aMnnRhTwd3bhqo5N+30XmSF5ZI3kVGuT17EW1Sl6p91x8CJJX1khehQZ5BYH5e5WU18n5tSvXtKHsZyF5rYGdaitUYsY/Gu298ASKI++rzV3XbLbd2dlZkJUVpAWUtEYj8gHi4qMGaUEsscHw3G1Xmq6zdxjuOXWgVG6Ec8bWTj21zog/R/LKCCoyUtupNV21WnfD4XApm6pFXJRkgxH5weRFtBXL637y7HoHQ/etVH253xaFMQ4GVgfieiHWh+S1BkKEFVVWPnEtGqPbWKs13OHhoZtMJrgqFBqGlWSDEfnB5IWw6Cpal/H6buq6+0chsiLa4n5TB5g6QdTNkXqhp5HZIXmtgeQTJyouy0ao3PuHx67fP3QXFxdLkRcNw0qywYj8EEdeJi+4Gz+54eml2+0NXLvbfznavC8+uKwexHVDrA/Jaw1Y14DKyvhH/+gsPIm6up242/GjG4+nLwuz46LB+vwTBuu9tExc1m3kPV3Hy5txuM+jq3t3eDwKY1/ft2uhO8mHGvUhWV/EepC81oANylJR+TSmMk98kGWVfVVBXto9KN8gL+Z5JeVl2Dm+fnP/6PYHJ2HY4O+tSoi+iMaS9UWsB8lrDSAvoi4iLj6J4yUksKqExqHoK9dY5GWS4oioYpnFUuODi+EConGwbqRYP5LXGmDAlu7C0clFGAuJKzUpVCg0BLqOwGsrcUMReeR1nhd3jdQ4U/+/e/8JxZH3ZJ2wr3M8O79yjd2eq9Za4YFNWp0Rf47ktQaIvBjvGD/+DFEXXUYqMTDSRQNIbxiiGMwFtur4/PzTPc7o/vtz/n5PpzPXHxy5er3p6g0JLCskrzWAvIi6kBbyiruNs0UlT28UYhOg68+TZI4UsuUyNabRaLiaF1itLnllgeS1BhigZaAeaTGRUfL6WlCQFw9f7DWTkptNxCV5ZYXktQZ4qsQ6N8TFQG48E1vy2nwoNp5JQWKnp6eu1Wq5ao3oS/LKAslrDTCfh9xONlAfy4sBXep3WqUXmwEFcdkkZM6NRiO3u+vrR7UueWWE5LUGWA7CJEUTFtGX5PV1oBBtxTtDXV1duU6n48qVmuSVEZLXGmCaBPO7TFjxvB/Ja/OhIC/byo5yc3Pjut2u5JUhktcaYP2a5PV1oSTldXt7K3lljOS1BpikypgXsqLLyHyvF5F5qN9plV5sBhTEZd1Gxr6Oj4/D00YN2GeH5LUGkBdPG4m4kJcmqX4tKIjL5MXRpkpIXtkhea0B1rDZJNXkVAnJa/OhICwiLt6Px2PX7/ddvc6TRs3zygrJaw0wz+vg8DRkFYifNBrU72SFF5sDxSapMsue8a5er+eq1WpYHiR5ZYPktQYsbxPTJZaiLl+vJa/Nh2Li4nh5eenabS+sSsU1mm3JKyMkrzVAql+mS7BEaGl2/SIKo36nVXqxGVAstRHyOj8/D13GnZ0dyStDJK81QM4mBEY+r6XZ9YvX1O9khRebA8WOzLQ/OTkJUVepVHLNlv9wk7wyQfJaA8yw50gWzVhetkCbep2s8GJzoNgReTFNgqhL8soWyWtNkPaXsS92lYnlpUmqmw9dRSt0GUmFQ+RF11ED9tkhea0J23AhnmkveX0NYnmRTQJp8aQRiUle2SF5rQl2jElmlwhzviSvjceSEHKkyyh5fQyS15ogISHyIh30w/THS/QleX0NKExUHQwGrlarBYGRz0vyyg7Ja00wURV5sQV8PO4leW0+Vu7v793e3t5L1MVcL8krOySvNYG4gAwTtkhb8voaUDheX1+HHF4M1rOukWSEkld2SF5rwLZ2R17M92KyquT1daAw3nVxcRG6iuVyORwRmeSVHZLXGojlxWvGvSSvrwPFZtbTXURedBklr2yRvNYAWSVsoipTJnjNIm3kpQ04Nh8KUyQQF9BdDIP1mueVKZLXGoi3dWfKBPJivlfI7yV5fQlYEoSsTF6MeYX3WtuYGZLXGqCryFgXr9mAFpGRnJCMquo2bj5kUWWTWZMXXUaOTJnQ8qDskLzWhI132Xwv8nsxZULy2nzYbGN/fz/ICmnRZeQ1UyZa7Y7klRGS1xpAVhyRF/C63e2709GNmz5LXpsO410Mztv8LrqMTJfgfXu3K3llhOS1BugyMu6FuBAZrzk/GJ67yRMVXPLaZEj5HPLVe1lxRGBklUBgu52e5JURktcaMHkBr+k6MnC/1z+WvL4ARF10E21yKmNf29vbklfGSF5rgAF6hGVPHdlN6K/v5ZAaejwjw6bkVWyc+xHuYfqx3Z7vjF3x8HSxXm+6rdKOK5errtPdc5WG5JUFklfW+IpLJY+LNQoryw1F5AvnHmfzfThnT85d30/cZPoc3j+MZ25vMJxHVv4+rzqm1gvxx0heWeMr793dw8ueflYsjQolvdGIPEBkZfIKAvO3zV4zl8/m94mPR/LKGi+v09PzsB1WLKy4pDUakQ+Q15M3lQkrhr06GdtMve8icySvrPHyOjgYuNFoFPb2SytpjUbkgzC25e8RsiLqMpjDx5jm/77tpN93kTmSV9Z4eTGAyyRGBPbw8OAmk0mYlW0blaY1GpEPTF73k+eQ6uj47CrAZitMi2Eta+p9F5kjeWWNl9fW1vyxOctG2En54ODADYfDsDkpCezSGo3IByYvxEWuNtatskcn99bm9r255+JDkLyyxsuLR+akSWHios3/YW4QKVSIwtIajcgHNmDPnD2mwCAsIi6mxoAG7D8PyStrvLy63b2w3g1pbW1tBYF1u92wJo6S1mhEPkBet3eTEHWxvZ3N5eOIvCwVkvh4JK+s8fJi8iKRFzOvkRjLSIjCWFbC2BfjXsDTSKDRxCXZoMR6YaNYG3uMC++ffzg3urhx3f2jICuyhrCKgrWriItzqfddZI7klTVeXtVq/Y28eM8gPgP4ktfngrxWXXfkdXZ+FZ4sIiq6jiYvW1mRet9F5kheWePlVavNUwOz/s3SpfCewXs2bZC8Phe79nHhPOeenn+64fF5kJV1FZEX3UjrPqbed5E5klfWeHk1GvPxLss6QASGvMi4eXZ2JnnlhLjwnnvyOHt2B/1heMLIk0VkhbzoMnKUvD4PyStrvLxaLX/08gJLmYK8OLJJqeT1uaSVEHX57uT08cntdvZfnipa2iPExVFTJT4PyStrvLx2d7tBXAgLeQGvicR46ih5fS5phfvBJOLJdObqjdeEk8Dr+L34HCSvrPHy6nT8p7aXF08YERfjXsjLZCZ5fS5phfvBYvrx5NGVKz5SXkRcJi1FXJ+P5JU1yIucTtW6l1cl5HuiG0n+J95Xaw3fUH6GgWHvrfB0i/YUN6m0BifWR1oJXcbp1D2Mp257x9+r6nxyKt1F7ivyMqG9uefiQ5C8PgIvMFiV74kxlfvp08sCYATGMbw3mSUanFgfVniNtKyw+kG7/+QXySsH8NjdNqkF9nu019r3MXus8JruohXm4Ele+UXyygE8hh9d3b8IK4Y4gPaVbHBifVjhNWOOVpiDJ3nlF8krByCv4ell2KQ2TWC0r2SDE+vDSiwuIjDm4GnH6/wieeUAJjySH4q0wnGXUfL6GCgcY3nRZWQOXr3RkrxyiuSVA5gAybgXSe7iHOkmMtpXssGJ9UGxaSpWyLXGHLxavSl55RTJKwcgLx7B94/O3KO3lcnLREb7Smt0Yj1QbK4dhXMnJydhDp7klV8krxyAuJgzRMK76dOrvExktK9kgxPrg2JpcShEYEdHR2ENquSVXySvHGCztclccHJ+vSSwqf8f7Sut0Yn1QGE2vW2QQmpuMt2yAkJjXvlF8soRPHU8PB65u7GPAnwjMmhfaY1OrAdKnNPr6uoqdBlZzqWnjflF8soBLDGxpSZk7Dy/vHMTHwRIXh+HFSTG5ihEXaTrbrU7kldOkbxyQLxejmkTDNyzL6Dk9XFYocvIU0bba6C9yxNHySuPSF45gKeNRF0s/kVgjH2djm40YP9BWKHbGCamNhquVCqFlEWSV36RvHKATZXgtWXqPDg8dbcPM8nrA7BC1LW3txfSF5Gqm302tTwov0heOQB50V3kaOfY8IGxLz1tzB6KRV22sxNdR0SmAfv8InnlAKTFk0awwXubcR9S5UheGRLc5Z6efrjB4Cjsbk6etX7/0A36x0FctZR7Jj4fySsHfCtV3fft2tKOzCa0Vnd/3roWhQaXLG8bpIj58YPcHCz9YQnQU3iiCP5L/uL5//yXRuc3rtnoutJW3VXKvrvY8FFXbd+V/WvJK59IXjmAjUwRF9tq2YRVxr1Cd3K352azt1tzxSWtwYpXYnnB/Jx/vZDX+OHJHQ9Hrl7dc9tb/sOjvOdajUP//sBtb3t51ebjkSJfSF45AGHFLH290Xaj0aUbj8e+pb0WlrLQCClxQxVvQV4/f/LowwS2KIuo6/Tk0u31Dr20uq687bvxXlrIq1bZd6US+2xKXnlE8soBRFlAd5H3CIzuI5HYdrXher19d3FxERqiFZsRTkk2VpEE0b+V1w9/ajJ+cp3dA9eo+2u/M6dR67tmfSB55RzJKwfYk0abLoG8bGv5rXLNN56GOzw8DDmmrCAuyet9IKw0eY0fZu5idOPqtV1XrbSCuIi+Ynnt7PgPF8krl0heOcDElRZ5lSp1V6nUwu7ap6enK7uPYjUmr1eBzYV/eXHru4teUtX2nIq/Dx66jUattid55RTJKwcgLRMX2NgXAtupNV21WvcNqBYyHdB9jAuZEJKNVSxj8noduJ9ft6PDU1fx0W2r2QtPGnm6iLwYsCfqIvpqtbzcJK9cInnlAJOVsfT1RtvV6687bB8fHy8a5LyQyiXZWMUyJq/nZ1LezOXFtmZEXX//VQpjXrvtfddq9IO8yts+CvYCQ16dzlDyyimSV97x8mKTWhYJIy8yfCIsjXn9E17Hu4i+6Gqzoezh4MR9+3s7jHkFqnsL1G0sApJX3knIi3EvujyS1z9hWV5cNz4A6DZ+/7YjeRUUySvveHk1m+0gL8a9zs/Pl9IVU9IbrDDiSarzOV8/wwfA8OjMlbYqkldBkbzyzkJeIatno7E03wt5xY1UpBPLax6FzZ/SnhyP3M52TfIqKJJX3vHyajRaQV6kJiZFsZW0hireEstrzvzanZ1ehqeNklcxkbzyzkJePG0kv9TNzU1ofJR5w1T09TtenzK+yosyOr92tWpL8iooklfeieTFRNW7u7tF05O83ssqeTFJteGvseRVTCSvvOPlZfO8kBfZPuMief2eVfK6urxzTdIPSV6FRPLKO15eTJVgA1Qye8bLg3hipkmqv2c2m/qrNRfXfPxrXug21mv+w0HyKiSSV96J5HVwcBBmhltBXpDWYMUrq+TFomx1G4uL5JV3FvJimsRgMAgzw62QFkcLs3/P09Ojv1pv5UW3sd3qSV4FRfLKO15ezPNqtVphM1QiLSvIi5LWYMUr8ZjX/PW83N6MXbfjJSV5FRLJK+94efG0kWkSLA2KZ9fb67QGK16xeV5MUJ1HYfNyfzd1B/tHkldBkbzyjpcXTxt50sjSIBojBXFJXu+F6zSX13z8a17Iosr6RsmrmEheeWchL3J5XV5eLprdXF5EX5T0BisM6zIm5fU4/eGOh+eSV0GRvPKOlxdpoNkENV4apBz278fkRfcxltfs8ac7PbmQvAqK5JV3vLxKpZ2Xwfp4rIvC67QGK14xeb0yl/908hzy2JMCulJuhp2DSEbIzkHt5lGQl3LY5xfJK+8s5EUG1TR50QjTGqx4JU1eXEO6jYx7sXOQbcARy4uNOMplf07yyiWSV97x8mL7eTKoWpRl3UWK5PV7VsmLbiPRV7vlu4q+22jyIv2z5JV/JK+84+XF7kFnZ2dLY1xWJK/fkyYvruXTbD5o3+30wyYciMvkRfSFvCqVruSVUySvvOPlxdPG0WgUGh1l3iDnRcuD3kO87dlcXpx/fpoP2h/sD8MGHAzW276NyAuJVateaJJXLpG88o6XFzPs42kScZG8fg+z6m2iaiwvdsxGYIeDU9frDsLWZ8iLgXqTl5425hfJK+94ebG28fr6OjS6ZJG8fg+z6pHXa/Q1Lz/9SwR2PBy5/T3GuF7lhbigXt+XvHKK5JV3vLza7c5SBtW4SF6/B3kRfb2Rl+99Iy9y2dN1ZOPZamXX1aod/3rPR199Ly/fbUy7L+LTkbxyALtlxxvO2tb/7Ji9Va65i4sr9/DwEBqiFV4z6Bw3UvFvcG46nbm9vYMwJYXdyeeZa6vz3bTbkldekbxyhMnLtv/n/Xa14a6ubkISQhqbFcQVP30U/x4SOvb7TIuY70pO+qFKpRJo+i57rZ7YxVzkAskrR5i0lt43d93t7f1SHi+KRV2UZGMU/wymm7CCgX0xTV68hrqPwiSvfCJ55QAiLERl3UWLwOqtnmt1932XcRLGtuIiea0PriVTUcjcgbzIWovAOFZrXmSSVy6RvHIAY1uIq9HeC0feI7Dm7r7bGwx9t+Z1ETaFBhcvEUo2RvHPoNze3rr9fZ4sziMu8qexT+ZOuSp55RTJKwcgqxBldQ6CvLYrvtH4c+1u3x2esL3/61gXBZFJXuuDQrf86OjoRV6kICJ77VZpR/LKKZJXDkBWyAtZIa9SuRHOdfYO3cnoNQ2OFclrvVC4pmSqRVx0HXu9XuhGfvtekrxyiuSVAxAVXUaT19ZOPZzr7h+5s8u387skr/VihUy1YZzLy4tt5pDX39+2JK+cInnlAMa3/t6quG+lapCXjXvt9Y/deMbAvASVJRQ+EBi0R147Ozuh20j0VanWJa+cInnlBMQFvLYpE4PhuZv6AIv2ldboxPqgMGiPtJjvxXhXePqop425RfLKAYx3Mc6FvOguco7B+9PRjaNzSNtKa3BiPRB1UWy+l3Udw3QJzfPKLZJXDmC8i27i9+1aGO+i27g/OHE394+S1wcQjx+yhpToi66jZtjnG8krByArjkRfwPyus4tbN31yktcHYJEXhcnATJkg8qL7KHnlF8krBxB12cx6YKD+bvwUxCV5ZU+ykH7IZttLXvlF8soBdBc52hKho5ML9+TbFOJ69EEB7Sut0Yn1QGG8yyIwoq/Dw8Mw5tVotiWvnCJ5fQTkofeERpBy/PZ9Jxybra5rdvbc+eh6HnF5Hmc0MMkrSyjMsCe7hBX2DOCJowbs84vklTVeSjQAHrmTI6q0XXbbnrBmzp/ja1tb276BNP2n/dDd3N6H5UDzJjUvaQ1OrA+7xkRfVngfFmt3mOs1X65l3XpeMzbJwxWLmsXHI3lljZcXCe5YdhK6IQt4z4AwT7VsLR3bm5F0MB5ApsQNTawfu8axvCjM+xr4D5TStv/Q8bJCWtxTG580ib255+JDkLyyxsuLrcuYO0SmAiTFYDDvt7e33bdv39zBwUHYVJaB4rjrYiXZ2MR6sRKPe1EY+7q+uVuKvOy+IrCl+yw+HMkra7y8QvfQy8qiLpsESdSFwC4uLsL8IsZdFHV9PFaQVzzni/Lsb8fR8Mz1DoYv8/GItphMjMA4l3rfReZIXlnj5cXuPwgLWZVKpSAs5EUkxvq5yWQSIq40cXEu2djEerGCuJLy4qsI7PpuGiYOE32xEoKxLuTFSojU+y4yR/LKmoW8GNdCWsiLmdvdbjekYKGrSIOJG5EVztnXRHZYSb6n8C4IzDO6ug+ZPrivRGCKvD4XyStrFt1G5GXjXmz2QFfR8tInGwyFc0Rdklf2JMe64hLLazJz7vJmHObhkWuNOXnxOJj4WCSvrPHy2tmpvDxRZOEv0VbyyRYlblA0JsnrY6DLnuwu2vX/4c3FXLuZdxvfAfeTZzc8vQxRl6ZKfB6S1zuInzTRVTDscXnySRTY19i6bH+/H54m3t3dhcZihdfaNDbfIC9gxcMq4gy4f30vB5gDRh1hnepub/CSOYSv/e/bzksGEb4nrjfi/Uhe78DkREWjklIR+dTlaK/5Gt9nT6L4c5xn9x/2Xby/v38TbfHJLnnlm/fI6/zyLkRiPJGkPnD/kRdQH0xQ8YedkfzQE+9H8nonSXHxiQo8beJrVEIqKvB9VGTycV3djReqeltoHOoW5pv3yIuuJBlASGF0fHYVBvWpE3QpibQ4Ui+oR1Z/bLzMPujEP0fyegdUNqtwVErDZMYnrHUB6CLwKXz7MHsZI/ldSWs0Ih+8V17Gw/RHeCrZPzp7+WCzCIxIK1m3FHn9eySvd8CYBlGWfVomQ37Exqct0mI+0KOvxXGFtkJjINKiuxiXZIMR+eE98ooH84H7T0qji+sHd3J+HeoPH3LUGaKwZCQW1zXxfiSvd8AnKILiUxRhASKzwVg+aekyJKVFV+Le/4+F1ox3GZJXcXiPvMaPP18SR8bwNeoEwwfsR4DEqDsWiVG3JK9/j+T1Dqy7yGuOyOzg8DR8qvLpSuVNiouKS4V+8F9AXgiLxpBWkg1G5If3Rl4c4/sfQ/1gGIHsuHQnLRKjPln0Lv45X0NeDV9BmCy6Ip/W745hYW6t6aq1lmvs9lx/cOzOr27d9PFHqJwP45m7m8zcePIUtip78ic57+tzYFWhcWjAPu8s5OXv16qjP7zcbztynmVFM//G3jNf7Pp+4k7PLl1nfxDq11a59vv6l1anxReQl7/55NJi/z3yaW3vVMKk0XKFbd3Z1qoZsj7w9bp/TeZMlvO02h3X9pAGuOnP7e333Wh06SbTma+Gi4rqI6rp49NLBU87zklrFKI4rL6/7zlafaEEoc2e/Qfe1N3fj0P+tl5vP9Qz6ic7dH/3UE+pj+3drqu355E/URrdTZuGEXc/vyJfQl5BTpVKWAwNzHYnu0Oz2QzZMpn5HrJmLrI9gC3lsTxbLOch15bNxKZSM37F7Oy3lV2IV+LC8AH1hqVh4/E41CmytrLpB7t0Uyepf1Zf+XBtNF/nifFU2/jqXc4vIS8+0UxIJizgNZBfiyMVhgSByI2F0yzlIZsmE0mte2eFSsg5TTIVv8MKr63eIDDqDiAyJMayMRbrk9/Ncr6FHoLvPiIqnlYiLZtyYVN13tT5L8LX6DYuMpnG4iLyQmiWzRRxUVls/SH5tahgyWIV0IgrqRBpxPUlrc5Y4WuIDImxnIxIjOELxlpNXHQTLeqiKyl5bTJeXo3GXFYGIkNWlgyQ93zSEbpTcfg0TBYqmX1ignUfKcnKKERMUly/K9QvlpMxVHF8chY2ZiHKoj6bxCwC+8pdxy8hLwbcTVphHMEfrQvJuBah+tXV1UuKGitUNBNVHOpzpCJaiSuqEKv4J8WisPuHScjkytQc0vAwt9AG701iqfX+C/BlIi+LtgBpMa7AQCnSSkuJQuGcjWnFxSojFYzvsfdCpPFvC/Xr6fmnm0yfwyRoFoAz2RWJWXfRxr++Il9CXkyJsG4ikRfjWojLIi2LrIz3VDi+hwhMTxvF71hVfvV1+1qYcuHf89HKukkEZhNdrSv5VSmEvOK1hXzSMGhpEDbbWjH7FCKsfqE5nz/DmBbdQ/LFx8UqiRB5BHkx6Tmewc+MftZOsiyNLBbUeRsLs3ZBt9KeRjIfjK/ZID/nDd7Hba1IFEJe3ACwi52Er3HkBtrkPV5zjhnxbOZKpEUyQKIlK1QOdftEnkmTF69ZckQSAARGWmo2B2HZGoKKBUYb4Eg74nUsLUi2tSJRCHnZxeeiW3oaMKFx5HuQFlEYnzJEagxyHp9fOmY0pw2yIy7OJyuMEHkhTV4mMHLqk5KadbWsnWStLdlNaBN8eBsmqphkGysihZBXHPLGURjnuDm2FRXfh9T4FGJsgDGC5EytUCEWA+3xGJcQeQR5saQoKS8jPk99J1HA4fFoaYMQaycxtCvaE9+TbG9FoVCRV/weUcWRFkIzafFpFN9UK+HpjY+0wKRFSVYYIfIC8goC8/V0FURecX2nS0l3Mu5K0kasO2nBAG2GHkrctopEIeRlnw52E8A+PfganzSkG2EQM76J3FRLSYOwbDlGWhdSiDyCuPx/L3U6DSIu6np8jnRMbNNmXcm0NDyS1wdgF90+Pbj4llOLpy1EWvT/44yWvOYc8np6mncTERYVIlmSFUaIvJCUFx/ORlzX4/fxeSTGh7rtN4nIGFrhQ1/dxvfQ8KZnvtVv8hatOrKqvlz3ffVy3ZUq9bBcoj88ddc3DyF/FnmSJv7u2XHmb5jlUYIoyHpT0iqMEPnhVV6I7BFR+Td2tHpuX59yztd/xnrtvB3vHx7d8MwLrNd3tZYXl29ftKvftb/UNp0DspdXuAjNAPmJ0o5bW9shfxGTScmnxXIecnBtb5dDfiO+zvuDg4EbXVy5yeQxDGJSuDHc4PAJlXKck1YphCgKq+v3e448becYt5fbuwd3fHwa8tSRuMDy2cXtMkzu9ufZe9TGy4AhGyI2upx0R1Pb/QfwIfLiYrCW8FfY8h2yPAAz4VmPyFKe/f39sGiaharkQNITQiHeD+O88RgvhXFfFn+zPI4J3Ml8drQ9znd7+67eSEz8XnQ3Gc6xIZ3PIBfyYrkOF4vlO1tbWyHTA9Lq9/thVjzCYikP0oqLPT1Mu2FCiDnxk/W0QvonJnGz3heJEUgAr3c7vTBsg6TiOZY2VmaD/59BLuQVp6nB+mR6sPQ0yeU8cZG8hPg97y1EYmQNJhEn7TEEE6WdsIcDkoqjLaIve/qf2u4/gFzIi4vExSJMHQwGIXspmSVNTL8qyRslhFgmLnzg/yoSI9HA5eVl6PUQVJS2y57q0vrIuH0r8vIXqdfrhS4i9k92D9NuQlzs60KIt8TFeisGbQ1hxWNivCZ4OD8/d4dHxy859Bmot3mWJq3PnGqRC3lxkayLmLzYq0QWf198TgixTFrhvEVhjCcjsmTh64+z57BVG3Mq44Xf1l388gP2qy4wF5YnJcmSvDHxeyHEMrQjjv+m8KfYn5RJruQRQ1Y2ZQKRFT7ywsj2BIJ/UBxSkpKGfRBtUN6ymNKvtqgqebGFEPmA+WFhUqyHFSuWvYKF33Qj//pefhEZYsMFln/PBvWTvlgXa5FXcv4HR84HkdVbYRIc8uIpBk8REVfcRUxeMCFEPkBeQJjBEiQERh6x4enly1IjJGaSMgfYU8ksu5VrkRf9X1sobRLjdRjgqzZ817AZBuSZS8KcLS5KXJIXTAiRDxAXrRV5GURhrJck/c7p6OZl0TcCIwrjyWQcicWuWCdrkRe/qD2JSA7mNTt7YRmCzY5PFgYF0y6aEOLziSOvGKIw60rSjWQ8jK4i7d/y65nAkr5YF2uRF6EjR4z791Yl/AM4xxOKk9GVF9Q8ukqW5IUSQuQLxMVCb0SVlrkCrCtJaioiLQIZXAC4IOmLdbE2edFVJOICbIuJeUJB1oe4EGnZHBNeU9IumhDi8zF5kVoHgYGl4IllxjkbzO8dDIMDcAERWJoz1sHaxrwIEZEYvzg5tjAxRk7OHuEJI1h3kZK8YEKIfGBjXogKQSGvpMhMYMDXrm4nYX9JfGBDSFnwPnk12oFV+X7++rsUBub39g/d+dVt2CTT8gwFcy82wFg13yR5wYQQeWHejhETR9p1iMQQFxLz8uJoXw8S8+cubu7dQX+4yBs298i/Pa7i9/LyctopV1/y/Vi+rXrYhXqe76fT6YVlBDc3d0t5tig23iWEKCrzIOTfHrudA++LjquUyRO26zq7B67DE8rarvu+VQlHzq86pnrJ8y55ISvmaTEbnommZH7gyDkmnbKQmrQarJFKFj1NFOIr49z52ZUb9E9cu7XnatW2q1Z84OOPQU6L4yqQ1yqBvUte3e5eyO2DtMi1RRYIRMakU1af80uuKm//MUKIr4PvVj7+dPd3j+54OHLNRtdtfa+479/KQWK77f1UaRl/LK/d3W6IspBWqVQKebds0unt7e1CU6tL+j9KCLH5IIA544cnd3R4FqSFvMo7jRCNpUnL+GN5hbEtLyzkRfRFxIW4WI0eF35Zuonxk0TK8j9GCPF1QACvPNzP3OHg1DXqnSCxNGHF/LG82AgDeVk+eVLYxBlOTVg8TUx7qvj2HySE+Bp4P9ijyEhgdCEZuE8TVswfy6tcrgZxkeWURdXJiCut8IvHUZgQ4iuCDN6CwE6OL1y3M3/quIo/lhfdRsa4eKqYFBdRVlrhFycK4/vf/oOEEF8DZDCPvp5m/iVTqRbvb67HYQwsTVrGb+XFgkpLZcOR9y/rFcu1MI8LcfHLWEFM9n75lxVCCGMurF+xv3f0Mo0CYfGap5C8Zx7YL+WFsAxLZ2Pn6+2uOzwcvnmqKHkJIX7PW1klOT25dAf7wzCNAmExmG+vt3fqv5YXq8CDqFrznDyktGBFOCvEB8dnYeZ8nI6ZcSzkZSX9lxZCiLeySsIUitH5jet1B0FYTKGwJ5G/7TYiKiIuW0j5v287YTX4Xv/YXd9PvKwWlloUe6poJf2XFkKIt7JKwhjYZPzshkfnIeLa2a4HgSGvlnfSL+VlaSssjQU5uYjAyA7BwstkQV7xQH36Ly2EEMuiSsOeQF5fPYTxL6RVKTdD97HNeHwkrJiXyAtZxeNd5KcmtUXas0T7xaws/7JCCGHMBfUrTF6zRxe6j3u9wyCuMPblA6rfjnkBAmPsi5xcZxe3IR9X+kSIebGxr/RfWgghlkWVhskLppMfYQCfCazI67djXnwxJBT0MO51fHIRsiKGtBb8TP8/m3AaF7qOZJJI/6WFEML7IyGrJM8MnwfRzLm+uvfdR6IvL6nqPI1OUlzwH77AyD7ioq/ZH5y4u9tp+Dn2w8IvsPQLCSHEezGHpPP0xNJC/2JRiJGur29DNput0o6X13xbtTfyQlj2aJLJYaw5YuTfxCV5CSGyhKGn+AEgPTzyA7KOulyp/VpePJrkyEj/1eX9UhjHa//z3/yFQgixDmxIitcUjvf392GD6maLYa3fyKvV7IWFknHURX+URGL8zPgvE0KIdRLLi8KaaJYk9gdHv5YXcyqY3UrUFaevQF5hMaXkJYTICArH+IEg3ci7uzt3Prr8tbyY1UqCMNJUmLhigfHzk3+hEEKsAyvJ6IslifcPk1/Li5w6zK1gjkVSXkFggfS/WAgh/gQrRFtx9EV5ev75a3kxUM/MVma4Sl5CiI/ESpq8+OpKebGK++z0yt3eTN5MFjP4+Wl/qRBCZAkT5Z+endvtDdz//VUKSxmZSM9KoP8wBf9idBvGuxjfehFW/DqQ/sOFECIrkBcbWbNk8a/v5WV5kbGQGfWPUwz1KiyiMJ48asBeCPFZmLwGw3NXKjcCiCvIi/EuBurTpkhAEJjkJYT4BJAXnI5uQsouEkhYuvr/kAAsOdaFsBBXmF2vyEsI8UkgLrREei66jmS+AUT2n/Oz66XxrVhedt7/jDc/VAghssbkNX786Y5OLkLURQacIK/krHqTV3zO/4w3P1QIIbLG5DXzgdT55V3oLjLuFeT13//+1wkhRLH4r/v/Z+yIZHhKazMAAAAASUVORK5CYII=";
          }
       });
    });
  }
}
