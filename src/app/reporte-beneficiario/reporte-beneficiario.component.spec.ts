import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteBeneficiarioComponent } from './reporte-beneficiario.component';

describe('ReporteBeneficiarioComponent', () => {
  let component: ReporteBeneficiarioComponent;
  let fixture: ComponentFixture<ReporteBeneficiarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteBeneficiarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteBeneficiarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
