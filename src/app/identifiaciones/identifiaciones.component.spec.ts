import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentifiacionesComponent } from './identifiaciones.component';

describe('IdentifiacionesComponent', () => {
  let component: IdentifiacionesComponent;
  let fixture: ComponentFixture<IdentifiacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentifiacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentifiacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
