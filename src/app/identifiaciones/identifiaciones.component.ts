import { Component, OnInit, NgZone } from '@angular/core';
declare const db: any;
@Component({
  selector: 'app-identifiaciones',
  templateUrl: './identifiaciones.component.html',
  styleUrls: ['./identifiaciones.component.css']
})
export class IdentifiacionesComponent implements OnInit {
lista = [];
  model = {
    id: null,
    nombre: null
  };
  constructor(private zone: NgZone) { }

  ngOnInit() {
    this.onLoadList();
  }

  onSubmit(){
      if(this.model.id !== null ){
        this.update();
      }else{
        this.create();
      }
  }
  onLoadList(){
    this.lista = [];
    let context = this;
    db.each("SELECT id_documento, nombre FROM TipoDocumento order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.lista.push({id: row.id_documento, nombre: row.nombre });
       });
    });
  }
  create(){
    let context = this;
     db.run("INSERT INTO TipoDocumento ( nombre) VALUES (?)", 
     [this.model.nombre ],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  setRow(index: number){
    this.model = {
      id: this.lista[index].id,
      nombre: this.lista[index].nombre
    };
  }
  update(){
    let context = this;
     db.run("UPDATE TipoDocumento SET nombre = ? WHERE id_documento = ? ", 
     [this.model.nombre, this.model.id],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  init(){
     this.model = {
      id: null,
      nombre: null
    };
  }
}
