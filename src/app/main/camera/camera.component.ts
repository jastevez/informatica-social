import { Component, Input, NgZone, OnDestroy, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
declare const db: any;
declare const WebCamera: any;
@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit, OnDestroy {
  capturar = false;
  enabled = false;
  save_success = false;
  save_error = false;
  message_error = '';
  num_id: number;
  image: string
  active = false;
  constructor(private zone: NgZone, private router: Router, private route: ActivatedRoute) {
    this.capturar = false;
    route.params.subscribe(params => {
          if (params['id']) {
            this.num_id = params['id'];
          }else{
            this.num_id = 1023;
          }
    });
   }

  ngOnInit() {
      //this.load();
      //this.loadExist();
  }
  ngOnDestroy() {
      console.log('detener');
      WebCamera.reset();
  }
  loadExist(){
    let context = this;
    db.each("SELECT count(*) as c FROM Foto where num_id=?", [this.num_id],
     function(err, row) {
       context.zone.run(() => {
          context.active = false;
          //context.enabled = false;
       });
    });
  }
  back(){
    this.router.navigate(['.registro', {update: 'S', id: this.num_id}]);
  }
  reset(){
    this.active = true;
    this.enabled = false;
     if(this.active){
       this.capturar = true;
        this.load();
      }
  }
  load(){
    if(!this.enabled){ // Start the camera !
     this.enabled = true;
     WebCamera.set({
			width: 480,
			height: 360,
			image_format: 'jpeg',
			jpeg_quality: 100
		  });
     WebCamera.attach('#camdemo');
     console.log("The camera has been started");
   }else{ // Disable the camera !
     this.enabled = false;
     WebCamera.reset();
    console.log("The camera has been disabled");
   }
  }
  onSubmit(){
    this.capturar = false;
    this.saveImage();
  }
  saveImage(){
    let context = this;
      WebCamera.snap(function(data_uri) {
        context.image = data_uri;
        context.active = false;
      });
  }
  save(){
    this.create(this.image);
    this.capturar = false;
  }
  create(base64: string){
    let context = this;
    context.save_success = false;
    context.save_error = false;
     db.run("INSERT OR REPLACE INTO Foto (num_id, foto) VALUES (?,?)", 
     [this.num_id, base64],(err)=>{
       context.zone.run(() => {
       if(err){
          context.message_error = err;
          context.save_error = true;
           throw err;
       }else{
          context.save_success = true;
       }
     });
     });
  }

}
