import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaDTO } from './../../dto/persona';
declare const db: any;
declare const $: any;
declare const jsPDF: any;
declare const base64: any;
@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {
  model: PersonaDTO[] = [];
  programa = [];
  field: string;
  val: string;
  prog: string;
  est: string;
  constructor(private zone: NgZone, private router: Router, private route: ActivatedRoute) { 
    this.loadPrograma();
    this.loadList(null);
  }
  loadList(filter: string){
    let context = this;
    context.model = [];
    console.log(this.est);
    db.each("SELECT Numero_documento as num, tipo_doc, nombre1, nombre2, apellido1, apellido2, fecha_nacimiento, sexo, estado, fecha_expedicion, programa FROM Persona where estado like ? " + (filter !== null?filter:""),[(this.est === null || this.est === 'T')?'%':this.est],
     function(err, row) {
       if(row !== null)
       context.zone.run(() => {
         let name = row.nombre1;
         if(row.nombre2 != null)
           name += ' ' + row.nombre2;
         if(row.apellido1 != null)
           name += ' ' + row.apellido1;
         if(row.apellido2 != null)
           name += ' ' + row.apellido2;
          context.model.push(new PersonaDTO(row.tipo_doc, row.num,null,name, null, null, null, null,row.sexo, row.fecha_nacimiento, row.programa, row.estado));
       });
    });
  }
  loadPrograma(){
    let context = this;
    db.each("SELECT id_programa, nombre FROM Programa where estado = 'A' ",
     function(err, row) {
       context.zone.run(() => {
          context.programa.push({id: row.id_programa, detalle: row.nombre});
       });
    });
  }
  findProgram(num: number): string{
    let prog = '';
    for(let i = 0; i < this.programa.length; i++){
        if(this.programa[i].id === num){
          prog = this.programa[i].detalle;
        }
    }
    return prog;
  }
  getEstado(val: string): string{
    if(val === 'I'){
      return "Inactivo";
    }else if(val === 'A'){
      return "Activo";
    }
    return "";
  }
  ngOnInit() {
  }
  onSubmit(){
    let query = "";
    if(this.field === '1'){
      query = " and ( upper(nombre1) like upper('%" + this.val +"%')";
      query += " or upper(nombre2) like upper('%" + this.val +"%')";
      query += " or upper(apellido1) like upper('%" + this.val +"%')";
      query += " or upper(apellido2) like upper('%" + this.val +"%') )";
    }else if(this.field === '2'){
      query = " and Numero_documento like '" + this.val + "%'";
    }else if(this.field === '3'){
      query = " and programa = "+this.prog;
    }
    this.loadList(query);
  }
  reset(){
    this.loadList(null);
  }
  imprimir(){
    //window.print();
    this.pruebaDivAPdf();
  }
  pruebaDivAPdf() {
    let context = this;
        var pdf = new jsPDF('p', 'pt', 'letter');
        let source = $('#window-pdf')[0];
        $( "#window-pdf" ).prepend( '<p id="arg1">Listado de Beneficiarios</p>' );
        $( "#window-pdf" ).prepend( '<p id="arg2">Fundación Roca de Sion</p>' );
        let specialElementHandlers = {
            '#bypassme': function (element, renderer) {
                return true
            }
        };
        let margins = {
            top: 80,
            bottom: 60,
            left: 40,
            width: 522
        };

        pdf.fromHTML(
            source, 
            margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width, 
                'elementHandlers': specialElementHandlers
            },

            function (dispose) {
              pdf.output('blob');
              //var out = pdf.output();
              pdf.save('beneficiario.pdf');
               $( "#arg1" ).remove();
               $( "#arg2" ).remove();
            }, margins
        );
    }
    viewDetail(id: number){
      this.router.navigate(['.reporte-beneficiario', {id: id}]);
    }
}
