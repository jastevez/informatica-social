import { RehabilitadoDTO } from '../../dto/rehabilitado';
import { Component, OnInit, NgZone, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonaDTO } from './../../dto/persona';
import { AdultoDTO } from "../../dto/adulto";
declare const db: any;
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  save_success = false;
  save_error = false;
  message_error = '';
  update = false;
  model: PersonaDTO = new PersonaDTO(null, null, null, null, null, null, null, null, null, null, null, null);
  adulto: AdultoDTO = new AdultoDTO(null, null, null, null, null, null, null, null);
  rehabilitado: RehabilitadoDTO = new RehabilitadoDTO( null, null, null, null);
  institucion_educativa: string;
  grado_cursado: string;
  religion: string;
  programa = [];
  tipo_id = [];
  constructor(private zone: NgZone, private router: Router, private route: ActivatedRoute) { 
    this.loadPrograma();
    this.loadId();
    route.params.subscribe(params => {
          if (params['update'] && params['id']) {
            this.update = true;
            this.model.numero_id = params['id'];
            this.getPersona();
          }
    });
  }

  ngOnInit() {
  }
  onSubmit(){
    db.run("BEGIN TRANSACTION");
    if(this.update){
      this.updateR();
    }else{
      this.create();
    }
    db.run("END");
  }
  loadPrograma(){
    let context = this;
    db.each("SELECT id_programa, nombre FROM Programa where estado = 'A' ",
     function(err, row) {
       context.zone.run(() => {
          context.programa.push({id: row.id_programa, detalle: row.nombre});
       });
    });
  }
  loadId(){
    let context = this;
    db.each("SELECT id_documento, nombre FROM TipoDocumento order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.tipo_id.push({id: row.id_documento, detalle: row.nombre });
       });
    });
  }
  create(){
    let context = this;
      context.save_success = false;
      context.message_error = '';
      context.save_success = false;
     db.run("INSERT INTO Persona (Numero_documento, tipo_doc, nombre1, nombre2, apellido1, apellido2, fecha_nacimiento, sexo, estado, fecha_expedicion, lugar_expedicion, programa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", 
     [this.model.numero_id, this.model.tipo_identificacion, this.model.nombre1, this.model.nombre2, this.model.apellido1, this.model.apellido2, this.model.fecha_nac ,this.model.sexo, 'A', this.model.fecha_exp, this.model.lugar_exp,this.model.programa],(err)=>{
       context.zone.run(() => {
       if(err){
           context.save_success = true;
           context.message_error = JSON.stringify(err);
           db.run('rollback');
           throw err;
       }else{
          context.save_success = true;
          if(Number(context.model.programa) === 1 || Number(context.model.programa) === 2 ){
            this.insertAdulto(context.model.numero_id);
            if(Number(context.model.programa) === 1 ){
              this.insertMadre(context.model.numero_id);
            }else if(Number(context.model.programa) === 2 ){
              this.insertRehabilitado(context.model.numero_id);
            }
          }else if(Number(context.model.programa) === 3 ){
            this.insertNino(context.model.numero_id);
          }
          context.model = new PersonaDTO(null, null, null, null, null, null, null, null, null, null, null, null);
       }
       console.log('response database Persona: ', context.save_error, context.message_error, context.save_success);
     });
     });
  }
  updateR(){
    let context = this;
      context.save_success = false;
      context.message_error = '';
      context.save_success = false;
     db.run("UPDATE Persona set tipo_doc = ?, nombre1 = ?, nombre2 = ?, apellido1 = ?, apellido2 = ?, fecha_nacimiento = ?, sexo = ?, estado = ?, fecha_expedicion = ?, lugar_expedicion = ?, programa = ? where Numero_documento = ? ", 
     [this.model.tipo_identificacion, this.model.nombre1, this.model.nombre2, this.model.apellido1, this.model.apellido2, this.model.fecha_nac ,this.model.sexo, this.model.estado, this.model.fecha_exp, this.model.lugar_exp,this.model.programa, this.model.numero_id],(err)=>{
       context.zone.run(() => {
       if(err){
           context.save_success = true;
           context.message_error = JSON.stringify(err);
           db.run('rollback');
           throw err;
       }else{
          context.save_success = true;
          if(Number(context.model.programa) === 1 || Number(context.model.programa) === 2 ){
            this.insertAdulto(context.model.numero_id);
            if(Number(context.model.programa) === 1 ){
              this.insertMadre(context.model.numero_id);
            }else if(Number(context.model.programa) === 2 ){
              console.log('rehabilitado');
              this.insertRehabilitado(context.model.numero_id);
            }
          }else if(Number(context.model.programa) === 3 ){
            this.insertNino(context.model.numero_id);
          }
          context.model = new PersonaDTO(null, context.model.numero_id, null, null, null, null, null, null, null, null, null, null);
       }
       console.log('response database', context.save_error, context.message_error, context.save_success);
     });
     });
  }
  //TODO: Adulto
  insertAdulto(id: number){
        let context = this;
      context.save_success = false;
      context.message_error = '';
      context.save_success = false;

     db.run("INSERT OR REPLACE INTO Adulto (telefono,celular,email,estado_civil,direccion_domicilio,barrio_domicilio,estudios_cursados,seguro_medico,num_doc) values(?,?,?,?,?,?,?,?,?) ", 
     [this.adulto.telefono_1, this.adulto.telefono_celular, this.adulto.cuenta_correo, this.adulto.estado_civil , this.adulto.direccion , this.adulto.barrio, this.adulto.estudios, this.adulto.seguro_medico, id],(err)=>{
       if(err){
         context.save_success = true;
         context.message_error = JSON.stringify(err);
         db.run('rollback');
           throw err;
       }else this.ok('adulto');
     });
  }
  insertNino(id: number){
    console.log('0000insert');
      let context = this;
      context.save_success = false;
      context.message_error = '';
      context.save_success = false;

     db.run("INSERT OR REPLACE INTO Nino (institucion_educativa,grado_cursado,num_doc) VALUES (?,?,?) ", 
     [this.institucion_educativa, this.grado_cursado, id],(err)=>{
       if(err){
         console.log('error', err);
         context.save_success = true;
         context.message_error = JSON.stringify(err);
         db.run('rollback');
           throw err;
       }else this.ok('niño');
     });
  }
  insertRehabilitado(id: number){
        let context = this;
      context.save_success = false;
      context.message_error = '';
      context.save_success = false;
      console.log(this.rehabilitado);
     db.run("INSERT OR REPLACE INTO Rehabilitados (tiempo_consumo,tipo_alucinogeno,compromiso,observaciones,num_doc) VALUES (?,?,?,?,?) ", 
     [this.rehabilitado.tiempo_consumo, this.rehabilitado.tipo_alucinogeno, this.rehabilitado.compromiso,
     this.rehabilitado.observaciones, id],(err)=>{
       if(err){
         context.save_success = true;
         context.message_error = JSON.stringify(err);
         db.run('rollback');
           throw err;
       }else this.ok('rehabilitado');
     });
  }
  insertMadre(id: number){
        let context = this;
      context.save_success = false;
      context.message_error = '';
      context.save_success = false;

     db.run("INSERT OR REPLACE INTO Madre (iglesia,num_doc) VALUES (?,?) ", 
     [this.religion, id],(err)=>{
       if(err){
         context.save_success = true;
         context.message_error = JSON.stringify(err);
         db.run('rollback');
           throw err;
       }else this.ok('madre');
     });
  }
  back(){
    this.router.navigate(['.reporte-beneficiario', { id: this.model.numero_id}]);
  }
  image(){
    this.router.navigate(['.camera', { id: this.model.numero_id}]);
  }
   //TODO: Selects
  getPersona(){
     let context = this;
    db.each("SELECT Numero_documento,tipo_doc,nombre1,nombre2,apellido1,apellido2,programa,fecha_nacimiento,sexo,estado,fecha_expedicion,lugar_expedicion FROM Persona where Numero_documento = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.model = new PersonaDTO(row.tipo_doc, row.Numero_documento, row.fecha_expedicion, row.nombre1, row.nombre2, row.apellido1, row.apellido2, row.lugar_expedicion, row.sexo, row.fecha_nacimiento, row.programa, row.estado);
          if(context.model.programa === 1 || context.model.programa === 2){
            context.getAdulto();
            if(context.model.programa === 1){
              context.getMadre();
            }else if(context.model.programa === 2){
              context.getRehabilitado();
            }
          }else if(context.model.programa === 3){
            context.getNino();
          }
       });
    });
  }
  getAdulto(){
     let context = this;
    db.each("SELECT telefono, celular, email, estado_civil, direccion_domicilio, barrio_domicilio, estudios_cursados, seguro_medico, num_doc FROM Adulto where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.adulto = new AdultoDTO(row.telefono, row.celular, row.email, row.estado_civil, row.direccion_domicilio, row.barrio_domicilio, row.estudios_cursados, row.seguro_medico);
       });
    });
  }
  getNino(){
     let context = this;
    db.each("SELECT institucion_educativa, grado_cursado, num_doc FROM Nino where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.institucion_educativa = row.institucion_educativa;
          context.grado_cursado = row.grado_cursado;
       });
    });
  }
  getRehabilitado(){
     let context = this;
    db.each("SELECT tiempo_consumo,tipo_alucinogeno,compromiso,observaciones,num_doc FROM Rehabilitados where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.rehabilitado = new RehabilitadoDTO(row.tiempo_consumo, row.tipo_alucinogeno, row.compromiso, row.observaciones);
       });
    });
  }
  getMadre(){
     let context = this;
    db.each("SELECT iglesia, num_doc FROM Madre where num_doc = ?",[this.model.numero_id],
     function(err, row) {
       context.zone.run(() => {
          context.religion = row.iglesia;
       });
    });
  }
  ok(st: string){
    console.log('ok insert: ' + st);
  }
}
