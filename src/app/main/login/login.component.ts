import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var electron: any; 
declare var sqlite3: any; 
declare var db: any;
declare const $:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    showerror = false;
    txt_user = '';
    txt_passwd = '';
    returnUrl: string;
  constructor(private zone: NgZone, private router: Router, private route: ActivatedRoute) {
      // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
   }

  ngOnInit() {
  }

checkUser() {
    let context = this;
    this.showerror = false;
    if (this.txt_user.length < 4) {
        $('#showuser').removeClass("hidden-msg");
    }
    if (this.txt_passwd.length < 4) {
        $('#showpassword').removeClass("hidden-msg");
    }
    if (this.txt_user.length >= 4 && this.txt_user.length >= 4) {
        // console.log('user', context.txt_user);
        db.all('SELECT id_login, nombre_usuario, contrasena from login where nombre_usuario = ? ', [context.txt_user], function(err, rows) {
            context.zone.run(() => {
            if(rows.length === 0){
                context.showerror = true;
            }else{
                if(context.txt_passwd === rows[0].contrasena){
                    context.txt_passwd = null;
                    context.txt_user = null;
                    localStorage.setItem('currentUser', JSON.stringify({user: context.txt_user, last_login: new Date()}));
                    context.router.navigate([context.returnUrl]);
                }else{
                    context.showerror = true;
                }
            }
            });
        });
    }
}

}
