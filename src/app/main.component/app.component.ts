import { AfterViewInit, Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare const mainWindow: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
    ngAfterViewInit(): void {
      
    }

  title = 'app works!';
  constructor(private router: Router, private route: ActivatedRoute){
    
  }
  change(){
    if(localStorage.getItem('currentUser')){
      this.cerrar();  
    }else{
      this.router.navigate(['.login']);
    }
  }
  cerrar() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['.login']);
  }
}
