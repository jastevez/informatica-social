import { Component, OnInit, NgZone } from '@angular/core';
declare const db: any;
@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.css']
})
export class ActividadesComponent implements OnInit {
  lista = [];
  model = {
    id: null,
    nombre: null,
    descripcion: null,
    fecha_asistencia: null
  };
  constructor(private zone: NgZone) { }

  ngOnInit() {
    this.onLoadList();
  }

  onSubmit(){
      if(this.model.id !== null ){
        this.update();
      }else{
        this.create();
      }
  }
  onLoadList(){
    this.lista = [];
    let context = this;
    db.each("SELECT id_actividad, nombre, descripcion, fecha FROM Actividad order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.lista.push({id: row.id_actividad, nombre: row.nombre, descripcion: row.descripcion, fecha_asistencia: row.fecha });
       });
    });
  }
  create(){
    let context = this;
     db.run("INSERT INTO Actividad ( nombre, descripcion, fecha ) VALUES (?,?,?)", 
     [this.model.nombre, this.model.descripcion, this.model.fecha_asistencia ],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  setRow(index: number){
    this.model = {
      id: this.lista[index].id,
      nombre: this.lista[index].nombre,
      descripcion: this.lista[index].descripcion,
      fecha_asistencia: this.lista[index].fecha_asistencia
    };
  }
  update(){
    let context = this;
     db.run("UPDATE Actividad SET nombre = ?, descripcion = ?, fecha = ? WHERE id_actividad = ? ", 
     [this.model.nombre, this.model.descripcion, this.model.fecha_asistencia, this.model.id],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  init(){
     this.model = {
      id: null,
      nombre: null,
      descripcion: null,
      fecha_asistencia: null
    };
  }
}
