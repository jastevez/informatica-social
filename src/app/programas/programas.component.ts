import { Component, OnInit, NgZone } from '@angular/core';
declare const db: any;
@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.css']
})
export class ProgramasComponent implements OnInit {
  lista = [];
  model = {
    id: null,
    nombre: null,
    descripcion: null,
    estado: null
  };
  constructor(private zone: NgZone) { }

  ngOnInit() {
    this.onLoadList();
  }

  onSubmit(){
      if(this.model.id !== null ){
        this.update();
      }else{
        this.create();
      }
  }
  onLoadList(){
    this.lista = [];
    let context = this;
    db.each("SELECT id_programa, nombre, descripcion, estado FROM Programa order by nombre",
     function(err, row) {
       context.zone.run(() => {
          context.lista.push({id: row.id_programa, nombre: row.nombre, descripcion: row.descripcion, estado: row.estado });
       });
    });
  }
  create(){
    let context = this;
     db.run("INSERT INTO Programa ( nombre, descripcion, estado ) VALUES (?,?,?)", 
     [this.model.nombre, this.model.descripcion, this.model.estado ],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  setRow(index: number){
    this.model = {
      id: this.lista[index].id,
      nombre: this.lista[index].nombre,
      descripcion: this.lista[index].descripcion,
      estado: this.lista[index].estado
    };
  }
  update(){
    let context = this;
     db.run("UPDATE Programa SET nombre = ?, descripcion = ?, estado = ? WHERE id_programa = ? ", 
     [this.model.nombre, this.model.descripcion, this.model.estado, this.model.id],(err)=>{
       context.zone.run(() => {
       if(err){
           console.log(JSON.stringify(err));
           throw err;
       }else{
          context.init();
          context.onLoadList();
       }
     });
     });
  }
  init(){
     this.model = {
      id: null,
      nombre: null,
      descripcion: null,
      estado: null
    };
  }
}
