import { InformaticaSocialPage } from './app.po';

describe('informatica-social App', () => {
  let page: InformaticaSocialPage;

  beforeEach(() => {
    page = new InformaticaSocialPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
